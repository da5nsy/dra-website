---
permalink: /contact/
layout: splash
header:
  overlay_color: "#7ca681"
author_profile: false
title: "Contact"
---

The Digital Research Academy is currently being built with a small team.

To get in touch, send a message to [Heidi Seibold](mailto:dra@heidiseibold.de).

<img src="/assets/images/DRA_Logo/DRA_LogoColourPos.png" alt="Digital Research Academy Logo." width="300" />
