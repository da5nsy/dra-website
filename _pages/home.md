---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: "/assets/images/header_people.jpg"
excerpt: "Quality Training for Quality Research"

feature_topics:
  - image_path: /assets/images/icon-os.png
    image_size: 70%
    title: "OPEN SCIENCE"
    excerpt: "
- Open / FAIR Materials  

- Good Research Practices 

- Social Change in Science
"
  - image_path: /assets/images/icon-dl.png
    image_size: 70%
    title: "DATA LITERACY"
    excerpt: "
- Data Science

- Data Management

- FAIR Principles
"
  - image_path: /assets/images/icon-rse.png
    image_size: 70%
    title: "RESEARCH SOFTWARE ENGINEERING"
    excerpt: "
- Reproducible Coding

- Software Project Management

- Open Source
"


feature_stakeholders:
   - image_path: /assets/images/learners.jpg
     title: "LEARNERS"
     btn_label: "Information for learners"
     btn_class: "btn--primary"
     url: "/learners"
     excerpt: "
You want to learn digital skills for research?

- Receive tailored, high-quality training

- Learn from skilled, fairly paid and happy trainers

- Enhance the quality of your research

"

   - image_path: /assets/images/trainer.jpg
     title: "TRAINERS"
     btn_label: "Information for trainers"
     btn_class: "btn--primary"
     url: "/trainers-join"
     excerpt: "
You want to share your skills with others?

- Become part of a supportive trainer community with a shared mission

- Bring in your individual expertise and teaching style

- Receive a fair pay for your work

- Enhance the quality of your training

"
   - image_path: /assets/images/organisation.jpg
     title: "ORGANISATIONS"
     btn_label: "Information for organisations"
     btn_class: "btn--primary"
     url: "/organisations"
     excerpt: "
Want to get training from the Digital Research Academy for your organisation/community? We are here to help!
"
 
---

**Disclaimer:** This is a beta website for the Digital Research Academy. Everything here is work in progress and may be subject to change. If you have feedback, please get in touch!
[Contact](/contact){: .btn .btn--primary}
{: .notice--primary}


# OUR TOPICS

{% include feature_row id="feature_topics" %}


Digital skills in research are needed more than ever. We want to make these skills accessible to researchers. For this we built a network of trainers that give excellent training on digital research skills. 


# YOU

{% include feature_row id="feature_stakeholders" type="left" %}

 


# WHY?

We started the Digital Research Academy to **improve the quality of research** by providing training for digital research skills.

Digital skills in research are needed more than ever, especially in growing fields like 

- Open Science
- Data Literacy
- Research Software Engineering

The Digital Research Academy adresses the following challenges:
- The research community needs **experts** in Open Science, Data Literacy, and
  Research Software Engineering
- Researchers need to **learn new skills** all the time and need targeted training
- Organisations struggle with **finding trainers**

The Digital Research Academy is our answer. We train new trainers, build a
trusted network of trainers, and provide training and workshops for the
research community and organisations. 


