---
permalink: /learners/
layout: splash
header:
  overlay_color: "#c34685"
author_profile: false
title: "Let's learn together"
---

We are currently building up processes and structures of the Digital Research Academy.

If you'd like to book a course with us, the best way is currently to send a message to [Heidi Seibold](dra@heidiseibold.de).

