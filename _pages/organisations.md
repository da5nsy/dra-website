---
permalink: /organisations/
layout: splash
header:
  overlay_color: "#c34685"
author_profile: false
title: "Improve research quality in your organisation"
---

Do you want to bring quality traing for quality research to your organisation? Work with us!

We offer two possibilities for you:

1. **Regular**: Pay per training.
2. **Digital Research Academy Membership**: yearly membership fee for a full package.

We are currently building up processes and structures of the Digital Research Academy.

If you'd like to book a training or membership, the best way is currently to send a message to [Heidi Seibold](dra@heidiseibold.de).

