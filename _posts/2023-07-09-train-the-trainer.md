---
title: "Train-the-trainer pilot program"
categories:
  - training
tags:
  - TTT
---

We are excited to announce our first train-the-trainer (TTT) workshops.

:tada:

## TTT 1, online

August 28 - September 1, 10:00 - 12:00 CEST, online

## TTT 2, Munich

September 4 - 6, 10:00 - 16:00 CEST, in Munich (board, lodging and travel self-paid)


## More information

- [About the TTT](https://gitlab.com/digital-research-academy)
- [How to sign up](https://heidiseibold.ck.page/posts/train-the-trainer-program)

